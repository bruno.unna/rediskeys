package biz.te2.poc;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.UUID;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class RedisHandlerTest {

  private static RedisHandler redisHandler;
  private static String payload;

  @BeforeAll
  static void setUp() throws IOException {
    redisHandler = new RedisHandler("localhost", 32768);
    URL url = RedisHandlerTest.class.getResource("/payload.json");
    payload = new String(Files.readAllBytes(Paths.get(url.getPath())));
    System.out.println("payload = " + payload);
  }

  @Test
  void set() {
    redisHandler.set("hello", "kitty");
    Assertions.assertEquals("kitty", redisHandler.get("hello"));
  }

  @Test
  void setTenThousandEntries() {
    redisHandler.delete("user:*:*");
    int readRecords = redisHandler.count("user:*:*");
    Assertions.assertEquals(0, readRecords);

    Instant now = Instant.now();
    IntStream.range(0, 10_000)
        .mapToObj(i -> {
          int delta = (int) (Math.random() * 1e+6);
          return String
              .format("user:%s:%d", UUID.randomUUID().toString(), now.toEpochMilli() + delta);
        })
        .forEach(key -> redisHandler.set(key, payload));

    readRecords = redisHandler.count("user:*:*");
    Assertions.assertEquals(10_000, readRecords);
  }

}
