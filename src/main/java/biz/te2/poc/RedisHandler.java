package biz.te2.poc;

import java.util.Objects;
import java.util.function.Consumer;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;

/**
 * Performs diverse operations on Redis, specially some related to key-handling.
 *
 * Created by bruno on 04/06/17.
 */
class RedisHandler {

  private Jedis jedis = null;

  RedisHandler(String host, int port) {
    jedis = new Jedis(host, port);
  }

  public void set(String key, String value) {
    jedis.set(key, value);
  }

  public String get(String key) {
    return jedis.get(key);
  }

  public int count(String pattern) {
    ScanParams params = new ScanParams().match(pattern);
    return operate(params, "0", s -> {
    });
  }

  public void delete(String pattern) {
    ScanParams params = new ScanParams().match(pattern);
    operate(params, "0", s -> jedis.del(s));
  }

  private int operate(ScanParams scanParams, String cursor, Consumer<String> function) {
    ScanResult<String> scanResult = jedis.scan(cursor, scanParams);
    int partialSize = scanResult.getResult().size();
    scanResult.getResult().forEach(function);
    if (Objects.equals(scanResult.getStringCursor(), "0")) {
      return partialSize;
    }
    // note the call is strictly tail-recursive
    return partialSize + operate(scanParams, scanResult.getStringCursor(), function);
  }

}
